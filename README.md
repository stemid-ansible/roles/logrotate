# Logrotate ansible role

* Template inspired by [Nick Hammond's role](https://github.com/nickhammond/ansible-logrotate).
* See defaults/main.yml for an example use case.

You can also specify a list of paths like this;

```yaml
logrotate_configs:
  - name: my-logs
    paths:
      - /var/log/remote/*.log
      - /var/lib/docker/containers/*/*.log
```
